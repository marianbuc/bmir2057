package bmir2057MV.exception;

public class DuplicateIntrebareException extends Exception {

    private static final long serialVersionUID = 1L;

    public DuplicateIntrebareException(String message) {
        super(message);
    }

}
