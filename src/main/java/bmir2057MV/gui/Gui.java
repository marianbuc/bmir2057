package bmir2057MV.gui;

import bmir2057MV.controller.AppController;
import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.exception.NotAbleToCreateStatisticsException;
import bmir2057MV.exception.NotAbleToCreateTestException;
import bmir2057MV.model.Intrebare;
import bmir2057MV.model.Quiz;
import bmir2057MV.model.Statistica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Gui {
    private String file;

    public Gui(String filename) {
        this.file = filename;
    }

    public void run() throws IOException {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        AppController appController = new AppController();

        boolean activ = true;
        String optiune = null;

        appController.loadIntrebariFromFile(file);

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    try {
                        Intrebare intrebare = readFromKeybord();
                        appController.addNewIntrebare(intrebare.getEnunt(), intrebare.getVarianta1(), intrebare.getVarianta2(), intrebare.getVarianta3(), intrebare.getVariantaCorecta(), intrebare.getDomeniu());
                    } catch (InputValidationFailedException | DuplicateIntrebareException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "2":
                    Quiz quiz;
                    try {
                        quiz = appController.createNewTest();
                        System.out.println(quiz);
                    } catch (NotAbleToCreateTestException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "3":
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println(e.getMessage());
                    }

                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }
    }

    private static Intrebare readFromKeybord() throws InputValidationFailedException {
        Scanner in = new Scanner(System.in);
        String intrebare, varianta1, varianta2, varianta3, raspunsCorect, domeniu;

        System.out.println("Introduceti intrebarea: ");
        intrebare = in.nextLine();

        System.out.println("Introduceti varianta 1: ");
        varianta1 = in.nextLine();

        System.out.println("Introduceti varianta 2: ");
        varianta2 = in.nextLine();

        System.out.println("Introduceti varianta 3: ");
        varianta3 = in.nextLine();

        System.out.println("Introduceti raspunsul corect: ");
        raspunsCorect = in.nextLine();

        System.out.println("Introduceti domeniul: ");
        domeniu = in.nextLine();

        return new Intrebare(intrebare, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
    }
}
