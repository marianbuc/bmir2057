package bmir2057MV.controller;

import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.exception.NotAbleToCreateStatisticsException;
import bmir2057MV.exception.NotAbleToCreateTestException;
import bmir2057MV.model.Intrebare;
import bmir2057MV.model.Quiz;
import bmir2057MV.model.Statistica;
import bmir2057MV.repository.IntrebariRepository;

import java.util.LinkedList;
import java.util.List;


public class AppController {

    public IntrebariRepository intrebariRepository;

    public AppController() {
        intrebariRepository = new IntrebariRepository();
    }

    public Intrebare addNewIntrebareIntrebare(Intrebare intrebare) throws DuplicateIntrebareException, InputValidationFailedException {
        return addNewIntrebare(intrebare.getEnunt(), intrebare.getVarianta1(), intrebare.getVarianta2(),
                intrebare.getVarianta3(), intrebare.getVariantaCorecta(), intrebare.getDomeniu());
    }

    public Intrebare addNewIntrebare(String enunt, String varianta1, String varianta2, String varianta3,
                                     String variantaCorecta, String domeniu) throws DuplicateIntrebareException, InputValidationFailedException {

        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);

        intrebariRepository.addIntrebare(intrebare);
        return intrebare;
    }

    public boolean exists(Intrebare intrebare) {
        return intrebariRepository.exists(intrebare);
    }

    public Quiz createNewTest() throws NotAbleToCreateTestException {
        if (intrebariRepository.getIntrebari().size() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui quiz!(5)");
        if (intrebariRepository.getNumberOfDistinctDomains() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui quiz!(5)");
        List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
        List<String> domenii = new LinkedList<String>();
        Intrebare intrebare;
        Quiz quiz = new Quiz();
        while (testIntrebari.size() != 5) {
            intrebare = intrebariRepository.pickRandomIntrebare();
            if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
                testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }
        }
        quiz.setIntrebari(testIntrebari);
        return quiz;
    }

    public void loadIntrebariFromFile(String f) {
        intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");

        Statistica statistica = new Statistica();
        for (String domeniu : intrebariRepository.getDistinctDomains()) {
            statistica.add(domeniu, intrebariRepository.getIntrebariByDomain(domeniu).size());
        }

        return statistica;
    }

    public void deleteAll() {
        intrebariRepository.deleteAll();
    }

    public void addIntrebareWithoutCheck(Intrebare i) {
        intrebariRepository.addIntrebareWithoutCheck(i);
    }

    public List<Intrebare> getIntrebari() {
        return intrebariRepository.getIntrebari();
    }
}
