package bmir2057MV.model;

import java.util.LinkedList;
import java.util.List;

public class Quiz {

    private List<Intrebare> intrebari;

    public Quiz() {
        intrebari = new LinkedList<Intrebare>();
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder();
        for (Intrebare intrebare : intrebari) {
            toReturn.append(intrebare).append("\n");
        }
        return toReturn.toString();
    }
}
