package bmir2057MV.repository;

import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.model.Intrebare;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class IntrebariRepository {

    private List<Intrebare> intrebari;

    public IntrebariRepository() {
        setIntrebari(new LinkedList<Intrebare>());
    }

    public void addIntrebare(Intrebare i) throws DuplicateIntrebareException {
        if (exists(i))
            throw new DuplicateIntrebareException("Intrebarea deja exista!");
        intrebari.add(i);
    }

    public void addIntrebareWithoutCheck(Intrebare i) {
        intrebari.add(i);
    }



    public boolean exists(Intrebare i) {
        for (Intrebare intrebare : intrebari)
            if (intrebare.equals(i))
                return true;
        return false;
    }

    public Intrebare pickRandomIntrebare() {
        Random random = new Random();
        return intrebari.get(random.nextInt(intrebari.size()));
    }

    public int getNumberOfDistinctDomains() {
        return getDistinctDomains().size();

    }

    public Set<String> getDistinctDomains() {
        Set<String> domains = new TreeSet<String>();
        for (Intrebare intrebre : intrebari)
            domains.add(intrebre.getDomeniu());
        return domains;
    }

    public List<Intrebare> getIntrebariByDomain(String domain) {
        List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
        for (Intrebare intrebare : intrebari) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebariByDomain.add(intrebare);
            }
        }

        return intrebariByDomain;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebariByDomain(domain).size();
    }

    public List<Intrebare> loadIntrebariFromFile(String f) {

        List<Intrebare> intrebari = new LinkedList<Intrebare>();
        BufferedReader br = null;
        String line = null;
        List<String> intrebareAux;
        Intrebare intrebare;

        try {
            br = new BufferedReader(new FileReader(f));
            line = br.readLine();
            while (line != null) {
                intrebareAux = new LinkedList<String>();
                while (!line.equals("##")) {
                    intrebareAux.add(line);
                    line = br.readLine();
                }
                intrebare = new Intrebare();
                intrebare.setEnunt(intrebareAux.get(0));
                intrebare.setVarianta1(intrebareAux.get(1));
                intrebare.setVarianta2(intrebareAux.get(2));
                intrebare.setVarianta3(intrebareAux.get(3));
                intrebare.setVariantaCorecta(intrebareAux.get(4));
                intrebare.setDomeniu(intrebareAux.get(5));
                intrebari.add(intrebare);
                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("IO Exception");
        } catch (InputValidationFailedException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("IO Exception");
            }
        }

        return intrebari;
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

    public void deleteAll(){
        this.intrebari = new LinkedList<>();
    }
}
