package bmir2057MV.main;

import bmir2057MV.gui.Gui;

import java.io.IOException;


//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "intrebari.txt";

    public static void main(String[] args) throws IOException {
        Gui gui = new Gui(file);
        gui.run();
    }

}
