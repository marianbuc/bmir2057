package bmir2057MV.controller;

import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.exception.NotAbleToCreateStatisticsException;
import bmir2057MV.exception.NotAbleToCreateTestException;
import bmir2057MV.model.Intrebare;
import bmir2057MV.model.Quiz;
import bmir2057MV.model.Statistica;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BigBang {
    private AppController controller;
    private Intrebare i1, i2, i3, i4, i5, i6;


    @Before
    public void setUp() throws Exception {
        controller = new AppController();
        i1 = new Intrebare("Ana are mere?", "1) Da", "2) Da Normal", "3) Nu", "1", "D1");
        i2 = new Intrebare("Ana are pere?", "1) Da", "2) Da Normal", "3) Nu", "2", "D2");
        i3 = new Intrebare("Ana are prune?", "1) Da", "2) Da Normal", "3) Nu", "3", "D3");
        i4 = new Intrebare("Ana are mure?", "1) Da", "2) Da Normal", "3) Nu", "1", "D4");
        i5 = new Intrebare("Ana are ciorba?", "1) Da", "2) Da Normal", "3) Nu", "2", "D5");
        i6 = new Intrebare("Ana are Castane?", "1) Da", "2) Da Normal", "3) Nu", "1", "D2");

    }

    @After
    public void tearDown() throws Exception {
        controller.deleteAll();
    }


    @Test
    public void unitA() {
        String varianta1 = "1) Da";
        String varianta2 = "2) Da Normal";
        String varianta3 = "3) Nu";
        String domeniu = "Mere";
        String enunt = "Ana are mere?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("unitA: " + e.getMessage());
            fail();
        }
    }

    @Test
    public void unitB() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);
            controller.addNewIntrebareIntrebare(i5);

            try {
                assertEquals(5, controller.createNewTest().getIntrebari().size());
            } catch (NotAbleToCreateTestException e) {
                System.out.println("unitB: " + e.getMessage());
                fail();
            }
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void unitC() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);
            controller.addNewIntrebareIntrebare(i5);
            controller.addIntrebareWithoutCheck(i6);

            try {
                Map<String, Integer> stats = controller.getStatistica().getIntrebariDomenii();
                assertTrue(stats.get("D1") == 1 && stats.get("D2") == 2 && stats.get("D3") == 1 && stats.get("D4") == 1 && stats.get("D5") == 1);
            } catch (NotAbleToCreateStatisticsException e) {
                fail();
                System.out.println("unitC: " + e.getMessage());
            }

        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void integration() {
        try {
            controller.addNewIntrebare("Ana are mere?", "1) Da", "2) Da Normal", "3) Nu", "1", "D1");
            controller.addNewIntrebare("Ana are pere?", "1) Da", "2) Da Normal", "3) Nu", "2", "D2");
            controller.addNewIntrebare("Ana are prune?", "1) Da", "2) Da Normal", "3) Nu", "3", "D3");
            controller.addNewIntrebare("Ana are mure?", "1) Da", "2) Da Normal", "3) Nu", "1", "D4");
            controller.addNewIntrebare("Ana are ciorba?", "1) Da", "2) Da Normal", "3) Nu", "2", "D5");
            controller.addNewIntrebare("Ana are Castane?", "1) Da", "2) Da Normal", "3) Nu", "1", "D2");

            Quiz q = controller.createNewTest();
            Map<String, Integer> stats = controller.getStatistica().getIntrebariDomenii();

            assertTrue(q.getIntrebari().size() == 5 && stats.get("D1") == 1 && stats.get("D2") == 2 && stats.get("D3") == 1 && stats.get("D4") == 1 && stats.get("D5") == 1);

        } catch (DuplicateIntrebareException | InputValidationFailedException |
                NotAbleToCreateTestException | NotAbleToCreateStatisticsException e) {
            System.out.println("Integrare BigBang: " + e.getMessage());
            fail();
        }
    }
}