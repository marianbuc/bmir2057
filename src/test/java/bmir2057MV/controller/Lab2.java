package bmir2057MV.controller;

import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.exception.NotAbleToCreateStatisticsException;
import bmir2057MV.exception.NotAbleToCreateTestException;
import bmir2057MV.model.Intrebare;
import bmir2057MV.model.Quiz;
import bmir2057MV.model.Statistica;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class Lab2 {
    private AppController controller;

    private Intrebare i1;
    private Intrebare i2;
    private Intrebare i3;
    private Intrebare i4;
    private Intrebare i5;
    private Intrebare i6;
    private Intrebare i7;

    @Before
    public void setUp() throws Exception {

        controller = new AppController();

        i1 = new Intrebare("Ana are mere?", "1) Da", "2) Da Normal", "3) Nu", "1", "D1");
        i2 = new Intrebare("Ana are pere?", "1) Da", "2) Da Normal", "3) Nu", "2", "D2");
        i3 = new Intrebare("Ana are prune?", "1) Da", "2) Da Normal", "3) Nu", "3", "D3");
        i4 = new Intrebare("Ana are mure?", "1) Da", "2) Da Normal", "3) Nu", "1", "D4");
        i5 = new Intrebare("Ana are ciorba?", "1) Da", "2) Da Normal", "3) Nu", "2", "D5");
        i6 = new Intrebare("Ana are mere?", "1) Da", "2) Da Normal", "3) Nu", "1", "D6");
        i7 = new Intrebare("Ana are Castane?", "1) Da", "2) Da Normal", "3) Nu", "1", "D2");

    }

    @Test
    public void F02_TC01() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);

            try {
                controller.createNewTest();
                fail();
            } catch (NotAbleToCreateTestException e) {
                System.out.println("F02_TC01: " + e.getMessage());
                assertTrue(true);
            }
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void F02_TC02() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);
            controller.addNewIntrebareIntrebare(i7);

            try {
                controller.createNewTest();
                fail();
            } catch (NotAbleToCreateTestException e) {
                System.out.println("F02_TC02: " + e.getMessage());
                assertTrue(true);
            }
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void F02_TC03() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);
            controller.addIntrebareWithoutCheck(i6);

            try {
                Quiz q = controller.createNewTest();
                List<Intrebare> intrebari = q.getIntrebari();
                assertEquals(5, intrebari.size());

            } catch (NotAbleToCreateTestException e) {
                System.out.println("F02_TC03: " + e.getMessage());
                fail();
            }
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void F02_TC04() {
        try {
            controller.addNewIntrebareIntrebare(i1);
            controller.addNewIntrebareIntrebare(i2);
            controller.addNewIntrebareIntrebare(i3);
            controller.addNewIntrebareIntrebare(i4);
            controller.addNewIntrebareIntrebare(i5);
            controller.addIntrebareWithoutCheck(i7);

            try {
                controller.createNewTest();
                assertTrue(true);
            } catch (NotAbleToCreateTestException e) {
                System.out.println("F02_TC03: " + e.getMessage());
                fail();
            }
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
    }


    @After
    public void tearDown() throws Exception {
        controller.deleteAll();
    }
}