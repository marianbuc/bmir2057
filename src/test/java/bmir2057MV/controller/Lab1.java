package bmir2057MV.controller;

import bmir2057MV.exception.DuplicateIntrebareException;
import bmir2057MV.exception.InputValidationFailedException;
import bmir2057MV.exception.NotAbleToCreateStatisticsException;
import bmir2057MV.model.Intrebare;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class Lab1 {
    private AppController controller;
    private String varianta1;
    private String varianta2;
    private String varianta3;
    private String domeniu;

    @Before
    public void setUp() {
        varianta1 = "1) Da";
        varianta2 = "2) Da Normal";
        varianta3 = "3) Nu";
        domeniu = "Mere";

        controller = new AppController();
    }

    @Test
    public void TC1_ECP() {
        String enunt = "Ana are mere?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void TC2_ECP() {
        String enunt = "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC2_ECP: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC3_ECP() {
        String enunt = "Ana are mere";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC3_ECP: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC4_ECP() {
        String enunt = "ana are mere?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC4_ECP: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC5_ECP() {
        String enunt = "";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC5_ECP: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC6_ECP() {
        String enunt = "Ana are mere?";
        String variantaCorecta = "";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC6_ECP: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC1_BVA() {
        String enunt = "E";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            fail();
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            System.out.println("TC1_BVA: " + e.getMessage());
            assertTrue(true);
        }
    }

    @Test
    public void TC2_BVA() {
        String enunt = "E?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            fail();
        }
    }

    @Test
    public void TC3_BVA() {
        String enunt = "AB?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            fail();
        }
    }

    @Test
    public void TC4_BVA() {
        String enunt = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEabc?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            fail();
        }
    }

    @Test
    public void TC5_BVA() {
        String enunt = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEab?";
        String variantaCorecta = "3";

        try {
            controller.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            assertTrue(true);
        } catch (DuplicateIntrebareException ignored) {
        } catch (InputValidationFailedException e) {
            fail();
        }
    }


//    @Test
//    public void LAB04_T1_V() {
//        try {
//            controller.addNewIntrebareIntrebare(i1);
//            controller.addNewIntrebareIntrebare(i2);
//            controller.addNewIntrebareIntrebare(i3);
//            controller.addNewIntrebareIntrebare(i4);
//            controller.addNewIntrebareIntrebare(i5);
//            controller.addIntrebareWithoutCheck(i7);
//
//            try {
//                Map<String, Integer> stats = controller.getStatistica().getIntrebariDomenii();
//                assertTrue(stats.get("D1") == 1 && stats.get("D2") == 2 && stats.get("D3") == 1 && stats.get("D4") == 1 && stats.get("D5") == 1);
//            } catch (NotAbleToCreateStatisticsException e) {
//                fail();
//                System.out.println("LAB04_T1_V: " + e.getMessage());
//            }
//
//        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void LAB04_T2_IV() {
//        try {
//            controller.getStatistica();
//            fail();
//        } catch (NotAbleToCreateStatisticsException e) {
//            System.out.println("LAB04_T1_IV: " + e.getMessage());
//            assertTrue(true);
//        }
//    }

    @After
    public void tearDown() throws Exception {
        controller.deleteAll();
    }
}